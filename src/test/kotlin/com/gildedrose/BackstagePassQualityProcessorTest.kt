package com.gildedrose

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

private const val ITEM_NAME = "Backstage Item"

class BackstagePassQualityProcessorTest {

    private lateinit var processor: IQualityProcessor

    @BeforeEach
    fun setup() {
        processor = BackstagePassQualityProcessor()
    }

    @Test
    fun `sellIn at 15, 1 quality update, expects quality increase of 1`() {
        // GIVEN

        val given = Item(ITEM_NAME, 15, 20)
        val expected = Item(ITEM_NAME, 14, 21)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 15 and quality at 50, 1 quality update, expects quality to remain unchanged`() {
        // GIVEN

        val given = Item(ITEM_NAME, 15, 50)
        val expected = Item(ITEM_NAME, 14, 50)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 10, 1 quality update, expects quality increase of 2`() {
        // GIVEN

        val given = Item(ITEM_NAME, 10, 4)
        val expected = Item(ITEM_NAME, 9, 6)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 10 and quality at 50, 1 quality update, expects quality to remain unchanged`() {
        // GIVEN

        val given = Item(ITEM_NAME, 10, 50)
        val expected = Item(ITEM_NAME, 9, 50)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 5, 1 quality update, expects quality increase of 3`() {
        // GIVEN

        val given = Item(ITEM_NAME, 5, 4)
        val expected = Item(ITEM_NAME, 4, 7)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 5 and quality at 50, 1 quality update, expects quality to remain unchanged`() {
        // GIVEN

        val given = Item(ITEM_NAME, 5, 50)
        val expected = Item(ITEM_NAME, 4, 50)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 0 and quality above 0, 1 quality update, expects quality drops to 0`() {
        // GIVEN

        val given = Item(ITEM_NAME, 0, 4)
        val expected = Item(ITEM_NAME, -1, 0)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at -1 and quality is 0, 1 quality update, expects quality to stay at 0`() {
        // GIVEN

        val given = Item(ITEM_NAME, 0, 0)
        val expected = Item(ITEM_NAME, -1, 0)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

}