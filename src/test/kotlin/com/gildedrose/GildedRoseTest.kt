package com.gildedrose

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

private const val CLASSIC_ITEM_1 = "Classic Item 1"
private const val CLASSIC_ITEM_2 = "Classic Item 2"

private const val AGED_BRIE = "Aged Brie"
private const val BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert"
private const val SULFURAS = "Sulfuras, Hand of Ragnaros"

class GildedRoseTest {

    @Test
    fun legacySafetyNet() {
        val items = arrayOf(Item(CLASSIC_ITEM_1, 10, 20),
                Item(AGED_BRIE, 2, 0),
                Item(CLASSIC_ITEM_2, 5, 7),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, 15, 20),
                Item(BACKSTAGE_PASSES, 10, 49),
                Item(BACKSTAGE_PASSES, 5, 49))

        val service = ItemService(ProcessorFactory())
        val app = GildedRose(items, service)

        // Day 1
        app.updateQuality()

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, 9, 19),
                Item(AGED_BRIE, 1, 1),
                Item(CLASSIC_ITEM_2, 4, 6),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, 14, 21),
                Item(BACKSTAGE_PASSES, 9, 50),
                Item(BACKSTAGE_PASSES, 4, 50)))

        // Day 5
        for (i in 0 until 4) {
            app.updateQuality()
        }

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, 5, 15),
                Item(AGED_BRIE, -3, 8),
                Item(CLASSIC_ITEM_2, 0, 2),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, 10, 25),
                Item(BACKSTAGE_PASSES, 5, 50),
                Item(BACKSTAGE_PASSES, 0, 50)))

        // Day 10
        for (i in 0 until 5) {
            app.updateQuality()
        }

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, 0, 10),
                Item(AGED_BRIE, -8, 18),
                Item(CLASSIC_ITEM_2, -5, 0),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, 5, 35),
                Item(BACKSTAGE_PASSES, 0, 50),
                Item(BACKSTAGE_PASSES, -5, 0)))

        // Day 15
        for (i in 0 until 5) {
            app.updateQuality()
        }

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, -5, 0),
                Item(AGED_BRIE, -13, 28),
                Item(CLASSIC_ITEM_2, -10, 0),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, 0, 50),
                Item(BACKSTAGE_PASSES, -5, 0),
                Item(BACKSTAGE_PASSES, -10, 0)))

        // Day 20
        for (i in 0 until 5) {
            app.updateQuality()
        }

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, -10, 0),
                Item(AGED_BRIE, -18, 38),
                Item(CLASSIC_ITEM_2, -15, 0),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, -5, 0),
                Item(BACKSTAGE_PASSES, -10, 0),
                Item(BACKSTAGE_PASSES, -15, 0)))

        // Day 30
        for (i in 0 until 10) {
            app.updateQuality()
        }

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, -20, 0),
                Item(AGED_BRIE, -28, 50),
                Item(CLASSIC_ITEM_2, -25, 0),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, -15, 0),
                Item(BACKSTAGE_PASSES, -20, 0),
                Item(BACKSTAGE_PASSES, -25, 0)))

        // Day 60
        for (i in 0 until 30) {
            app.updateQuality()
        }

        assertThat(app.items).isEqualTo(arrayOf(Item(CLASSIC_ITEM_1, -50, 0),
                Item(AGED_BRIE, -58, 50),
                Item(CLASSIC_ITEM_2, -55, 0),
                Item(SULFURAS, 0, 80),
                Item(SULFURAS, -1, 80),
                Item(BACKSTAGE_PASSES, -45, 0),
                Item(BACKSTAGE_PASSES, -50, 0),
                Item(BACKSTAGE_PASSES, -55, 0)))
    }
}


