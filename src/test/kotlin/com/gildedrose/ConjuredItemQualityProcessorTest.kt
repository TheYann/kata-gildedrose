package com.gildedrose

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

private const val ITEM_NAME = "Conjured Item"

class ConjuredItemQualityProcessorTest {

    private lateinit var processor: IQualityProcessor

    @BeforeEach
    fun setup() {
        processor = ConjuredItemQualityProcessor()
    }

    @Test
    fun `sellIn above 1, 1 quality update, expects quality decrease of 2`() {
        // GIVEN

        val given = Item(ITEM_NAME, 2, 4)
        val expected = Item(ITEM_NAME, 1, 2)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn above 0, 1 quality update, expects quality decrease of 2`() {
        // GIVEN

        val given = Item(ITEM_NAME, 1, 4)
        val expected = Item(ITEM_NAME, 0, 2)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 0, 1 quality update, expects quality decrease of 4`() {
        // GIVEN

        val given = Item(ITEM_NAME, 0, 4)
        val expected = Item(ITEM_NAME, -1, 0)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at -1, 1 quality update, expects quality decrease of 4`() {
        // GIVEN

        val given = Item(ITEM_NAME, -1, 4)
        val expected = Item(ITEM_NAME, -2, 0)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at -1 and quality at 1, 1 quality update, expects quality to 0`() {
        // GIVEN

        val given = Item(ITEM_NAME, -1, 1)
        val expected = Item(ITEM_NAME, -2, 0)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }
}