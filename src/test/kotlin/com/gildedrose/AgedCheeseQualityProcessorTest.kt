package com.gildedrose

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

private const val ITEM_NAME = "Aged Brie Item"

class AgedCheeseQualityProcessorTest {

    private lateinit var processor: IQualityProcessor

    @BeforeEach
    fun setup() {
        processor = AgedCheeseQualityProcessor()
    }

    @Test
    fun `sellIn above 1, 1 quality update, expects quality increase of 1`() {
        // GIVEN

        val given = Item(ITEM_NAME, 2, 4)
        val expected = Item(ITEM_NAME, 1, 5)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn above 0, 1 quality update, expects quality increase of 1`() {
        // GIVEN

        val given = Item(ITEM_NAME, 1, 4)
        val expected = Item(ITEM_NAME, 0, 5)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 0, 1 quality update, expects quality increase of 2`() {
        // GIVEN

        val given = Item(ITEM_NAME, 0, 4)
        val expected = Item(ITEM_NAME, -1, 6)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at -1, 1 quality update, expects quality increase of 2`() {
        // GIVEN

        val given = Item(ITEM_NAME, -1, 4)
        val expected = Item(ITEM_NAME, -2, 6)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 5 and quality at 50, 1 quality update, expects quality unchanged`() {
        // GIVEN

        val given = Item(ITEM_NAME, 5, 50)
        val expected = Item(ITEM_NAME, 4, 50)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at -1 and quality at 50, 1 quality update, expects quality unchanged`() {
        // GIVEN

        val given = Item(ITEM_NAME, -1, 50)
        val expected = Item(ITEM_NAME, -2, 50)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

}