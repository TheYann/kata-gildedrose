package com.gildedrose

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

private const val ITEM_NAME = "Legendary Item"

class LegendaryItemQualityProcessorTest {

    private lateinit var processor: IQualityProcessor

    @BeforeEach
    fun setup() {
        processor = LegendaryItemQualityProcessor()
    }

    @Test
    fun `sellIn at 10, 1 quality update, expects neither sellIn not quality to change`() {
        // GIVEN

        val given = Item(ITEM_NAME, 10, 80)
        val expected = Item(ITEM_NAME, 10, 80)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 1, 1 quality update, expects neither sellIn not quality to change`() {
        // GIVEN

        val given = Item(ITEM_NAME, 1, 80)
        val expected = Item(ITEM_NAME, 1, 80)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 0, 1 quality update, expects neither sellIn not quality to change`() {
        // GIVEN

        val given = Item(ITEM_NAME, 0, 80)
        val expected = Item(ITEM_NAME, 0, 80)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at -1, 1 quality update, expects neither sellIn not quality to change`() {
        // GIVEN

        val given = Item(ITEM_NAME, -1, 80)
        val expected = Item(ITEM_NAME, -1, 80)

        // WHEN

        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sellIn at 10, 50 quality update, expects neither sellIn not quality to change`() {
        // GIVEN

        val given = Item(ITEM_NAME, 10, 80)
        val expected = Item(ITEM_NAME, 10, 80)

        // WHEN

        for (i in 0 until 49) {
            processor.updateQuality(given)
        }
        val actual = processor.updateQuality(given)

        // THEN

        Assertions.assertThat(actual).isEqualTo(expected)
    }
}