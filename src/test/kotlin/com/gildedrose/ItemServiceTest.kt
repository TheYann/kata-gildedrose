package com.gildedrose

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.mockito.Mockito.verify

class ItemServiceTest {

    private lateinit var factory: IProcessorFactory
    private lateinit var service: IItemService

    @BeforeEach
    fun setup() {
        factory = mock()
        service = ItemService(factory)
    }

    @DisplayName("decorateItemTest()")
    @ParameterizedTest(name = "#{index}. item with name {0} and quality {1} expects = {2}")
    @CsvSource(
            "Aged Brie, 40, AGED_CHEESE",
            "aged brie, 40, AGED_CHEESE",
            "AGED BRIE, 40, AGED_CHEESE",
            "aged brie with a hint of sulfure, 40, AGED_CHEESE",
            "Aged Gouda, 40, DEFAULT",
            "Aged Bri, 40, DEFAULT",
            "Semi Aged Brie, 40, DEFAULT",
            "Backstage passes to a TAFKAL80ETC concert, 30, BACKSTAGE_PASS",
            "backstage pass, 30, BACKSTAGE_PASS",
            "BACKSTAGE PASSES to another concert, 30, BACKSTAGE_PASS",
            "Backstage to a TAFKAL80ETC concert, 30, DEFAULT",
            "Backst Passes to a concert, 30, DEFAULT",
            "'Sulfuras, Hand of Ragnaros', 80, LEGENDARY",
            "Sulfuras, 80, LEGENDARY",
            "sulfuras, 80, LEGENDARY",
            "SULFURAS, 80, LEGENDARY",
            "Some other High quality item, 80, LEGENDARY",
            "Conjured item, 15, CONJURED",
            "CONJURED item, 15, CONJURED",
            "Possessed item, 15, DEFAULT"
    )
    fun decorateItemTest(givenName: String, givenQuality: Int, expectedType: ItemType) {
        // GIVEN

        val item = Item(givenName, 5, givenQuality)
        val expected = ItemWrapper(item, expectedType)

        // WHEN

        val actual = service.decorateItem(item)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun decorateItemsTest() {
        // GIVEN

        val given = arrayOf(Item("foo", 0, 0),
                Item("Aged Brie", 0, 0),
                Item("Backstage Passes", 0, 0),
                Item("Sulfuras", 0, 80),
                Item("Conjured sausage", 0, 0))

        val expected = listOf(ItemWrapper(Item("foo", 0, 0), ItemType.DEFAULT),
                ItemWrapper(Item("Aged Brie", 0, 0), ItemType.AGED_CHEESE),
                ItemWrapper(Item("Backstage Passes", 0, 0), ItemType.BACKSTAGE_PASS),
                ItemWrapper(Item("Sulfuras", 0, 80), ItemType.LEGENDARY),
                ItemWrapper(Item("Conjured sausage", 0, 0), ItemType.CONJURED))

        // WHEN

        val actual = service.decorateItems(given)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `updateQualityTest, expects factory method to be called`() {
        // GIVEN

        val item = Item("foo", 0, 0)
        val wrapped = ItemWrapper(item, ItemType.DEFAULT)

        val processor = mock<IQualityProcessor>()

        whenever(factory.findProcessorForItem(wrapped)).thenReturn(processor)

        // WHEN

        service.updateQuality(wrapped)

        // THEN

        verify(factory).findProcessorForItem(wrapped)
        verify(processor).updateQuality(item)
    }
}