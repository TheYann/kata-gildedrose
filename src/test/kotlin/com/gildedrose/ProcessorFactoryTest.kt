package com.gildedrose

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ProcessorFactoryTest {

    private lateinit var factory: IProcessorFactory

    @BeforeEach
    fun setup() {
        factory = ProcessorFactory()
    }

    @Test
    fun `findProcessorForItem, with item type DEFAULT, expects DefaultQualiyProcessor instance, same instance each time`() {
        findProcessorForItemTestFixture(ItemType.DEFAULT, DefaultQualityProcessor::class.java)
    }

    @Test
    fun `findProcessorForItem, with item type BACKSTAGE_PASS, expects BackstagePassQualiyProcessor instance, same instance each time`() {
        findProcessorForItemTestFixture(ItemType.BACKSTAGE_PASS, BackstagePassQualityProcessor::class.java)
    }

    @Test
    fun `findProcessorForItem, with item type AGED_CHEESE, expects AgedCheeseQualityProcessor instance, same instance each time`() {
        findProcessorForItemTestFixture(ItemType.AGED_CHEESE, AgedCheeseQualityProcessor::class.java)
    }

    @Test
    fun `findProcessorForItem, with item type LEGENDARY, expects LegendaryItemQualityProcessor instance, same instance each time`() {
        findProcessorForItemTestFixture(ItemType.LEGENDARY, LegendaryItemQualityProcessor::class.java)
    }

    @Test
    fun `findProcessorForItem, with item type CONJURED, expects ConjuredItemQualityProcessor instance, same instance each time`() {
        findProcessorForItemTestFixture(ItemType.CONJURED, ConjuredItemQualityProcessor::class.java)
    }

    private fun findProcessorForItemTestFixture(givenType: ItemType, expectedClass: Class<*>) {
        // GIVEN

        val item1 = ItemWrapper(Item("foo", 0, 0), givenType)
        val item2 = ItemWrapper(Item("bar", 0, 0), givenType)

        // WHEN

        val actual1 = factory.findProcessorForItem(item1)
        val actual2 = factory.findProcessorForItem(item2)

        // THEN

        assertThat(actual2).isExactlyInstanceOf(expectedClass)
        assertThat(actual1).isExactlyInstanceOf(expectedClass)
        assertThat(actual1).isSameAs(actual2)
    }
}