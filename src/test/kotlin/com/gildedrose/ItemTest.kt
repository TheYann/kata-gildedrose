package com.gildedrose

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class ItemTest {

    @DisplayName("isAgedBrie()")
    @ParameterizedTest(name = "#{index}. item with name {0} expects = {1}")
    @CsvSource(
            "Aged Brie, true",
            "aged brie, true",
            "AGED BRIE, true",
            "aged brie with a hint of sulfure, true",
            "Aged Gouda, false",
            "Aged Bri, false",
            "Semi Aged Brie, false"
    )
    fun isAgedBrieTest(givenName: String, expected: Boolean) {
        genericItemTestFixture(givenName, expected) { item -> item.isAgedCheese() }
    }

    @DisplayName("isBackstagePass()")
    @ParameterizedTest(name = "#{index}. item with name {0} expects = {1}")
    @CsvSource(
            "Backstage passes to a TAFKAL80ETC concert, true",
            "backstage pass, true",
            "BACKSTAGE PASSES to another concert, true",
            "Backstage to a TAFKAL80ETC concert, false",
            "aged brie, false",
            "Backst Passes to a concert, false"
    )
    fun isBackstagePassTest(givenName: String, expected: Boolean) {
        genericItemTestFixture(givenName, expected) { item -> item.isBackstagePass() }
    }

    @DisplayName("isLegendaryItem()")
    @ParameterizedTest(name = "#{index}. item with name {0} and quality {1}, expects = {2}")
    @CsvSource(
            "'Sulfuras, Hand of Ragnaros', 80, true",
            "Sulfuras, 80, true",
            "sulfuras, 80, true",
            "SULFURAS, 80, true",
            "Anything else at this point, 80, true",
            "Anything else at this point, 15, false",
            "Sulfura, 15, false"
    )
    fun isLegendaryItemTest(givenName: String, quality: Int, expected: Boolean) {
        genericItemTestFixture(givenName, expected, quality) { item -> item.isLegendary() }
    }

    @DisplayName("isConjuredItem()")
    @ParameterizedTest(name = "#{index}. item with name {0} expects = {1}")
    @CsvSource(
            "Conjured item, true",
            "CONJURED item, true",
            "Possessed item, false"
    )
    fun isConjuredItemTest(givenName: String, expected: Boolean) {
        genericItemTestFixture(givenName, expected) { item -> item.isConjured() }
    }

    private fun genericItemTestFixture(name: String, expected: Boolean, quality: Int = 10, actionToTest: (Item) -> Boolean) {
        // GIVEN

        val item = Item(name, 10, quality)

        // WHEN

        val actual = actionToTest(item)

        // THEN

        assertThat(actual).isEqualTo(expected)
    }
}