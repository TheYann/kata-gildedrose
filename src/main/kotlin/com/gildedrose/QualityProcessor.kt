package com.gildedrose

/**
 * General rules for items:
 * - updateQuality is called once per item at end of day and decrease both sellIn and quality values
 * - Quality is never negative
 * - Quality never goes beyond 50
 *
 * Exceptions to be found in specific processors if any
 */

private const val MAX_QUALITY = 50
private const val BACKSTAGE_DOUBLE = 10
private const val BACKSTAGE_TRIPLE = 5

interface IQualityProcessor {
    fun updateQuality(item: Item): Item
}

fun Item.decrementQualityUntilMinimum(amount: Int = 1) {
    quality -= amount
    if (quality < 0) {
        quality = 0
    }
}
fun Item.incrementQualityUntilMaximum(amount: Int = 1) {
    quality += amount
    if (quality > MAX_QUALITY) {
        quality = MAX_QUALITY
    }
}

class DefaultQualityProcessor: IQualityProcessor {

    override fun updateQuality(item: Item) = item.apply {
        decrementQualityUntilMinimum()
        sellIn--
        if (sellIn < 0) {
            decrementQualityUntilMinimum()
        }
    }

}

class AgedCheeseQualityProcessor: IQualityProcessor {

    /**
     * Exception: quality actually increases the older it gets
     */

    override fun updateQuality(item: Item) = item.apply {
        incrementQualityUntilMaximum()
        sellIn--
        if (sellIn < 0) {
            incrementQualityUntilMaximum()
        }
    }

}

class BackstagePassQualityProcessor: IQualityProcessor {

    /**
     * Exceptions:
     * - quality increases the closer the sellIn value approaches
     * - quality increases by 2 if there are 10 or less days and by 3 if there are 5 or less days remaining
     * - quality drops instantly to 0 after the concert
     */
    override fun updateQuality(item: Item) = item.apply {
        sellIn--
        when {
            sellIn < 0 -> quality = 0
            sellIn < BACKSTAGE_TRIPLE -> incrementQualityUntilMaximum(3)
            sellIn < BACKSTAGE_DOUBLE -> incrementQualityUntilMaximum(2)
            else -> incrementQualityUntilMaximum()
        }
    }
}

class LegendaryItemQualityProcessor: IQualityProcessor {

    /**
     * Exception: Legendary items never have to be sold and quality never changes
     */
    override fun updateQuality(item: Item) = item

}

class ConjuredItemQualityProcessor: IQualityProcessor {

    /**
     * Exception: Conjured item's quality degrades twice as fast as normal items
     */
    override fun updateQuality(item: Item) = item.apply {
        decrementQualityUntilMinimum(2)
        sellIn--
        if (sellIn < 0) {
            decrementQualityUntilMinimum(2)
        }
    }

}