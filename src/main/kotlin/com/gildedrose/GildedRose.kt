package com.gildedrose

class GildedRose(var items: Array<Item>, private val service: IItemService) {

    private var wrappedItems: List<ItemWrapper> = service.decorateItems(items)

    fun updateQuality() {
        wrappedItems.forEach {
            service.updateQuality(it)
        }
    }

}

