package com.gildedrose

// This class cannot change, it makes Goblin angry!
data class Item(var name: String, var sellIn: Int, var quality: Int)
