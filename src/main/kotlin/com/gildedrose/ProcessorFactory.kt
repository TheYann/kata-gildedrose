package com.gildedrose


interface IProcessorFactory {
    fun findProcessorForItem(item: ItemWrapper): IQualityProcessor
}

class ProcessorFactory: IProcessorFactory {

    private val defaultProcessor by lazy { DefaultQualityProcessor() }
    private val agedBrieProcessor by lazy { AgedCheeseQualityProcessor() }
    private val backstagePassProcessor by lazy { BackstagePassQualityProcessor() }
    private val legendaryItemProcessor by lazy { LegendaryItemQualityProcessor() }
    private val conjuredItemProcessor by lazy { ConjuredItemQualityProcessor() }

    override fun findProcessorForItem(item: ItemWrapper) = when(item.type) {
        ItemType.AGED_CHEESE -> agedBrieProcessor
        ItemType.BACKSTAGE_PASS -> backstagePassProcessor
        ItemType.LEGENDARY -> legendaryItemProcessor
        ItemType.CONJURED -> conjuredItemProcessor
        else -> defaultProcessor
    }
}