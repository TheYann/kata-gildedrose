package com.gildedrose

enum class ItemType {
    DEFAULT,
    AGED_CHEESE,
    BACKSTAGE_PASS,
    LEGENDARY,
    CONJURED
}

data class ItemWrapper(val item: Item, val type: ItemType)

interface IItemService {
    fun decorateItems(items: Array<Item>): List<ItemWrapper>
    fun decorateItem(item: Item): ItemWrapper
    fun updateQuality(item: ItemWrapper)
}

class ItemService(private val processorFactory: IProcessorFactory): IItemService {
    override fun decorateItems(items: Array<Item>) = items.map { decorateItem(it) }

    override fun decorateItem(item: Item) = ItemWrapper(item,
            when {
                item.isAgedCheese() -> ItemType.AGED_CHEESE
                item.isBackstagePass() -> ItemType.BACKSTAGE_PASS
                item.isLegendary() -> ItemType.LEGENDARY
                item.isConjured() -> ItemType.CONJURED
                else -> ItemType.DEFAULT
            })

    override fun updateQuality(item: ItemWrapper) {
        val processor = processorFactory.findProcessorForItem(item)
        processor.updateQuality(item.item)
    }
}

private const val AGED_BRIE_PREFIX = "Aged Brie"
private const val BACKSTAGE_PASS_PREFIX = "Backstage pass"
private const val SULFURAS_PREFIX = "Sulfuras"
private const val CONJURED_PREFIX = "Conjured"
private const val LEGENDARY_QUALITY = 80

fun Item.isAgedCheese(): Boolean = name.startsWith(AGED_BRIE_PREFIX, true)

fun Item.isBackstagePass(): Boolean = name.startsWith(BACKSTAGE_PASS_PREFIX, true)

fun Item.isLegendary(): Boolean = name.startsWith(SULFURAS_PREFIX, true) || quality == LEGENDARY_QUALITY

fun Item.isConjured(): Boolean = name.startsWith(CONJURED_PREFIX, true)
